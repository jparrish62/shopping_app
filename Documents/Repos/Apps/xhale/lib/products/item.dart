import 'package:meta/meta.dart';

ItemList itemList = ItemList(items: [
	Item(
	 id: 1, 
	 title: "Sutra Mini Vaporizer", 
	 price: 89.99,
	 imgUrl: "assets/images/one.png"
  ),

	Item(
	 id: 2, 
	 title: "Huni Badger Vertical Vaporizer", 
	 price: 149.99,
	 imgUrl: "assets/images/two.png"
  ),

	Item(
	 id: 3, 
	 title: "The Peak Atomizer", 
	 price: 39.99,
	 imgUrl: "assets/images/three.png"
  ),

	Item(
	 id: 4, 
	 title: "Yocan Evolve Plus Wax Vaporizer Pen", 
	 price: 34.99,
	 imgUrl: "assets/images/four.png"
  ),

	Item(
	 id: 5, 
	 title: "Yocan Evolve Three In One Vape Pen Kit", 
	 price: 59.99,
	 imgUrl: "assets/images/five.png"
  ),

	Item(
	 id: 6, 
	 title: "Puffco Peak", 
	 price: 379.99,
	 imgUrl: "assets/images/six.png"
  ),
]);

class ItemList {
	List<Item> items;
	ItemList({@required this.items});
}

class Item {
	int id;
	String title;
	double price;
	String imgUrl;
	int quantity;

	Item({
	  @required this.id,
		@required this.title,
    @required this.price,
    @required this.imgUrl,
		this.quantity = 1,
    }
  );

	void incrementQuantity() {
		this.quantity = this.quantity + 1;
	}

	void decrementQuantity() {
		this.quantity = this.quantity - 1;
	}
}
