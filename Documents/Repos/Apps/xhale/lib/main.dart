import 'products/item.dart';
import 'bloc/cartListBloc.dart';
import 'package:flutter/material.dart';
import 'package:bloc_pattern/bloc_pattern.dart';

void main() => runApp(App());

class App extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return BlocProvider(
	  	blocs: [
				Bloc((i) => CartListBloc())
		   ],
			child: MaterialApp(
				title: "Xhale", 
				home: Home(), 
				debugShowCheckedModeBanner: false,
			)
		);
	}
}

class Home extends StatelessWidget {
	@override
	Widget build(BuildContext context) {
		return Scaffold(
      body: SafeArea(
        child: Container(
					child: ListView(
						children: <Widget>[
							FirstHalf(),
							SizedBox(height: 45),
							for(var item in itemList.items)
							ItemContainer(item : item)
						],
					),
				),
			),
		);
	}
}

class ItemContainer extends StatelessWidget {

	final Item item;
	ItemContainer({@required this.item});

	final CartListBloc bloc = BlocProvider.getBloc<CartListBloc>();
	addToCart(Item item) {
		bloc.addToList(item);
	}

	@override
	Widget build(BuildContext context) {
		return GestureDetector(
			onTap: () {
        addToCart(item);
				final snackbar = SnackBar(
						content: Text("${item.title} added to the cart"),
						duration: Duration(milliseconds: 550), 
			 );
				Scaffold.of(context).showSnackBar(snackbar);
			}, 
			child: Items(
				name: item.title, 
				price: item.price, 
				imgUrl: item.imgUrl,
				leftAligned: (item.id % 2 == 0) ? true : false
			),
		);
	}
}

class Items extends StatelessWidget {
	Items({
		@required this.leftAligned,
		@required this.imgUrl, 
		@required this.name,
		@required this.price, 
	});
  
	final bool leftAligned; 
	final String imgUrl; 
	final double price;
  final String name;

	@override
	Widget build(BuildContext context) {
		double contentPadding = 45;
		double containeerBorderRadius = 10;

		return Column(
			children: <Widget>[
				Container(
					padding: EdgeInsets.only(
						left: leftAligned ? 0 : contentPadding, 
						right: leftAligned ? contentPadding : 0,
				  ), 
			child: Column(
					children: <Widget>[
						Container(
								width: double.infinity, 
								height: 200, 
								decoration: 
									BoxDecoration(borderRadius: BorderRadius.circular(10)),
								child: ClipRRect(
									borderRadius: BorderRadius.horizontal(
											left: leftAligned ? Radius.circular(0)
														: Radius.circular(containeerBorderRadius),
											right: leftAligned ? Radius.circular(containeerBorderRadius)
														: Radius.circular(0),
									),
									child: Image.asset(
											imgUrl, 
											fit: BoxFit.fill,
											), 
								),
							),
						SizedBox(height: 20),
						Container(
								padding: EdgeInsets.only(
										left:  leftAligned ? 20 : 0, 
										right: leftAligned ? 0 : 20, 
										),
								child: Column(
										crossAxisAlignment: CrossAxisAlignment.start, 
										children: <Widget> [
											Row(
												children: <Widget>[
													Expanded(
														child: Text(
																name, 
															 style: TextStyle(
																fontWeight: FontWeight.w700, 
																fontSize: 18
															 ),
														 )
													 ),
													Text("\$$price", 
														style: TextStyle(
															fontWeight: FontWeight.w700, 
															fontSize: 18
												  	)
												  )
												],
											)
									],
								),
						  )
						],
					)
			  )
			],
		);
	}
}

class FirstHalf extends StatelessWidget{
	@override
	Widget build(BuildContext context) {
		return Padding(
      padding: EdgeInsets.fromLTRB(35, 25, 0, 0), 
			child: Column(
        children: <Widget> [
					CustomAppBar(),
					SizedBox(height: 30), 
					title(), 
					SizedBox(height: 30),
					searchBar(),
					SizedBox(height: 45),
					categories(), 
				],
		  )
		);
	}
}
 
Widget categories() {
  return Container(
    height: 185,
		child: ListView(
      scrollDirection: Axis.horizontal, 
			children: <Widget>[
				CategoryListItem(
				  categoryIcon: Icons.add_shopping_cart, 
					categoryName: "Pipes", 
					availability: 12,
					selected: true, 
			  ), 
				CategoryListItem(
				  categoryIcon: Icons.add_shopping_cart, 
					categoryName: "CBD", 
					availability: 12,
					selected: false, 
			  ),
				CategoryListItem(
				  categoryIcon: Icons.add_shopping_cart, 
					categoryName: "Vape Oil", 
					availability: 12,
					selected: false, 
			  ),
				CategoryListItem(
				  categoryIcon: Icons.add_shopping_cart, 
					categoryName: "Grinders", 
					availability: 12,
					selected: false, 
			  ),
				CategoryListItem(
				  categoryIcon: Icons.add_shopping_cart, 
					categoryName: "Glass Products", 
					availability: 12,
					selected: false, 
			  ),
				CategoryListItem(
				  categoryIcon: Icons.add_shopping_cart, 
					categoryName: "Replacement Pods", 
					availability: 12,
					selected: false, 
			  )
			],
		), 
	);
}

Widget searchBar() {
	return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween, 
		children: <Widget>[
			Icon(Icons.search,
					color: Colors.black45,
			),
			SizedBox(width: 20,),
			Expanded(
				child: TextField(
					decoration: InputDecoration(
							hintText: "Search...", 
							contentPadding: EdgeInsets.symmetric(vertical: 10),
							hintStyle: TextStyle(
               color: Colors.black87
						),
					),
				),
			)
		],
	);
}


Widget title() {
	return Row(
   mainAxisAlignment: MainAxisAlignment.start, 
	 children: <Widget>[
		 SizedBox(width: 45), 
		 Column(
      crossAxisAlignment: CrossAxisAlignment.start,
			children: <Widget>[
        Text(
          "Xhale City",
					style: TextStyle(
							fontWeight: FontWeight.w700,
							fontSize: 30,
					  )
					),
				Text("Delivery", 
						style: TextStyle(
              fontWeight: FontWeight.w200,
							fontSize: 30,
								)
						)
			  ],
		  )
	  ],
	);
}

class CustomAppBar extends StatelessWidget {

	@override
  Widget build(BuildContext context) {
	  final CartListBloc bloc = BlocProvider.getBloc<CartListBloc>();
		return Container(
      margin: EdgeInsets.only(bottom: 15), 
			child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween, 
				children: <Widget>[
					Icon(Icons.menu), 
					StreamBuilder(
				    stream: bloc.listStream,
						builder: (context, snapshot) {
							List<Item> item = snapshot.data;
							int length = item != null ? item.length : 0;
							return buildGestureDetector(length, context, item);
						},
				  )
				],
		  ),
	  );
	}
	GestureDetector buildGestureDetector(
		int length, BuildContext context, List<Item> item) {
		return GestureDetector(
			onTap: () {},
			  child: Container(
			  	margin: EdgeInsets.only(right: 30), 
					child: Text(length.toString()),
					padding: EdgeInsets.all(15),
					decoration: BoxDecoration(
				  color: Colors.yellow[800], borderRadius: BorderRadius.circular(50)
			  ),
		  ),
		);
	}
}


class CategoryListItem extends StatelessWidget {
  const CategoryListItem({
    Key key,
    @required this.categoryIcon,
    @required this.categoryName,
    @required this.availability,
    @required this.selected,
  }) : super(key: key);

  final IconData categoryIcon;
  final String categoryName;
  final int availability;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 20),
      padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        //color: selected ? Themes.color : Colors.white,
        border: Border.all(
            color: selected ? Colors.transparent : Colors.grey[200],
           width: 1.5),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[100],
            blurRadius: 15,
            offset: Offset(15, 0),
            spreadRadius: 5,
          )
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50),
                border: Border.all(
                    color: selected ? Colors.transparent : Colors.grey[200],
                    width: 1.5)),
            child: Icon(
              categoryIcon,
              color: Colors.black,
              size: 30,
            ),
          ),
          SizedBox(height: 10),
          Text(
            categoryName,
            style: TextStyle(
                fontWeight: FontWeight.w700, color: Colors.black, fontSize: 15),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 6, 0, 10),
            width: 1.5,
            height: 15,
            color: Colors.black26,
          ),
          Text(
            availability.toString(),
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.black,
            ),
          )
        ],
      ),
    );
  }
} 








