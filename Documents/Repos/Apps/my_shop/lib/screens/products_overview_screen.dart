import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../widgets/app_drawer.dart';
import './cart_screen.dart';
import '../widgets/products_grid.dart';
import '../providers/product.dart';
import '../providers/products_provider.dart';
import '../widgets/badge.dart';
import '../providers/cart.dart';

enum FilterOptions { Favorites, All, }

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreen createState() => _ProductsOverviewScreen();
}

class _ProductsOverviewScreen extends State<ProductsOverviewScreen> {
	var _showOnlyFavorites = false;
	var _isInit = true;
	var _isLoading = false;

  @override
  void initState() {
    // Provider.of<Products>(context).fetchAndSetProducts(); // WON'T WORK!
    // Future.delayed(Duration.zero).then((_) {
    //   Provider.of<Products>(context).fetchAndSetProducts();
    // });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Products>(context).fetchAndSetProducts().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

	@override
	Widget build(BuildContext context) {
		return Scaffold(
      appBar: AppBar(
			  title: Text('Xhale City'),
				actions: <Widget>[
					PopupMenuButton(
						onSelected: (FilterOptions selectedValue) {
								setState(() {
								if (selectedValue == FilterOptions.Favorites) {
									_showOnlyFavorites = true;
								}else {
									_showOnlyFavorites = false;
								}
							});
						},
						icon: Icon(Icons.more_vert,
					), 
				  	itemBuilder: (_) => [
						  PopupMenuItem(child: Text('Only Favorite'), value: FilterOptions.Favorites),
						  PopupMenuItem(child: Text('Show All'), value: FilterOptions.All 
							),
					  ],
					),
					Consumer<Cart>(builder: (_, cart, ch) => Badge(
							child: ch,
							value: cart.itemCount.toString(),
					),
							child: IconButton(
									icon: Icon(
											Icons.shopping_cart,
											),
									 onPressed: () {
										 Navigator.of(context).pushNamed(CartScreen.routeName);
									 },
									),
							),
				],
			),
      drawer: AppDrawer(),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ProductsGrid(_showOnlyFavorites),
		);
	}
}










