import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/http_exception.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'product.dart';

class Products with ChangeNotifier {
	List<Product> _items = [];
  
	final String authToken;
	final String userId;

	Products(this.authToken, this.userId, this._items);

	var _showFavoritesOnly = false;

	List<Product> get items {
		return [..._items];
	}

	List<Product> get favoriteItems {
		return _items.where((prodItem) => prodItem.isFavorite).toList();
	}

	Product findById(String id) {
		return _items.firstWhere((prod) => prod.id == id);
	}

  Future<void> fetchAndSetProducts() async {
		var url = 'https://flutter-update-a55cb.firebaseio.com/products.json?auth=$authToken&orderBy="creatorId"&equalTo="$userId"';
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
			if(extractedData == null) {
				return;
			}
		final fav_url = 'https://flutter-update-a55cb.firebaseio.com/userFavorites/$userId.json?auth=$authToken';
			final favoriteResponse = await http.get(fav_url);
      final favoriteData = json.decode(favoriteResponse.body);
      final List<Product> loadedProducts = [];
      extractedData.forEach((prodId, prodData) {
        loadedProducts.add(Product(
          id: prodId,
          title: prodData['title'],
          description: prodData['description'],
          price: prodData['price'],
          isFavorite: favoriteData == null ? false : favoriteData[prodId] ?? false,
          imageUrl: prodData['imageUrl'],
        ));
      });
      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }

  Future<Null> addProduct(Product product) async {
    final url = 'https://flutter-update-a55cb.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.post(
        url,
        body: json.encode({
          'title': product.title,
          'description': product.description,
          'imageUrl': product.imageUrl,
          'price': product.price,
					'creatorId': userId,
        }),
      );
      final newProduct = Product(
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
        id: json.decode(response.body)['name'],
      );
      _items.add(newProduct);
      // _items.insert(0, newProduct); // at the start of the list
      notifyListeners();
    } catch (error) {
			print('This the error $error');
      throw error;
    }
  }

	void showFavoritesOnly() {
		_showFavoritesOnly = true;
	}

	void showAll() {
		_showFavoritesOnly = false;
	}

	Future<void> updateProduct(String id, Product newProduct) async {
    final prodIndex = items.indexWhere((prod) => prod.id == id);
		if (prodIndex >= 0) {
		  final url = 'https://flutter-update-a55cb.firebaseio.com/products/$id.json?auth=$authToken';
			await http.patch(url, body: json.encode({
				'title': newProduct.title,
				'description': newProduct.description,
				'imageUrl': newProduct.imageUrl,
				'price': newProduct.price,
			}));
			_items[prodIndex] = newProduct;
			notifyListeners();
		}else {
			print('...');
		}
	}

	Future<void> deleteProduct(id) async {
		final url = 'https://flutter-update-a55cb.firebaseio.com/products/$id.json?auth=$authToken';
		final exitingProductIndex = _items.indexWhere((prod) => prod.id == id);
		var  exitingProduct = _items[exitingProductIndex];
		_items.removeAt(exitingProductIndex);
		notifyListeners();
		final response = await http.delete(url);
		if (response.statusCode >= 400) {
			items.insert(exitingProductIndex, exitingProduct);
		  notifyListeners();
			throw HttpException('Could not delete product');
		}
		exitingProduct = null;
	}
}









